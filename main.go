package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"github.com/disintegration/imaging"
)

var (
	input  string
	width  int
	height int
)

func init() {
	flag.StringVar(&input, "i", ".", "Input path to a file or folder.")
	flag.IntVar(&width, "w", 0, "Target image width. Can be omitted to maintain aspect ratio.")
	flag.IntVar(&height, "h", 0, "Target image height. Can be ommitted to maintain aspect ratio.")
	flag.Parse()
}

func main() {
	file, err := os.Open(input)
	if err != nil {
		fmt.Println("ERROR: input path does not exist:", err)
		os.Exit(1)
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		fmt.Println("ERROR: cannot open input path:", err)
		os.Exit(1)
	}
	filenames := make([]string, 0)
	if stat.IsDir() {
		files, err := os.ReadDir(input)
		if err != nil {
			fmt.Println("ERROR: cannot open input path:", err)
			os.Exit(1)
		}
		for _, file := range files {
			if _, err := imaging.FormatFromFilename(file.Name()); err != nil {
				continue
			}
			filenames = append(filenames, filepath.Join(input, file.Name()))
		}
	} else {
		filenames = append(filenames, input)
	}
	for _, filename := range filenames {
		fmt.Println("Processing", filename)
		img, err := imaging.Open(filename, imaging.AutoOrientation(true))
		if err != nil {
			fmt.Println("ERROR: cannot open image:", err)
			continue
		}
		if width > 0 || height > 0 {
			img = imaging.Resize(img, width, height, imaging.Lanczos)
		}
		if err := imaging.Save(img, filename); err != nil {
			fmt.Println("ERROR: cannot save image:", err)
		}
	}

}
