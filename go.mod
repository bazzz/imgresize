module gitlab.com/bazzz/imgresize

go 1.20

require github.com/disintegration/imaging v1.6.2

require golang.org/x/image v0.6.0 // indirect
