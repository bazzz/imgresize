# imgresize

Package imgresize is a wrapper around the imaging package that can be used to call resize operations on image from the command line.

Usage of imgresize:

-i string Input path to a file or folder. (default ".")

-h int Target image height. Can be ommitted to maintain aspect ratio.

-w int Target image width. Can be omitted to maintain aspect ratio.